<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class FeatureCollectionController extends Controller
{
    public function index(){
        $arr = [
            'type' => 'FeatureCollection',
            'features' => []
        ];
        $users = DB::table('feature_collections')->get();
        foreach ($users as $user){
            $coordinates = explode(',', $user->coordinates);
            $arr1 = [
                'type' => 'Feature','id' => $user->id,
                'geometry' => ['type' => 'Point','coordinates' => array_map('floatval', $coordinates)],
                'properties' => ['balloonContent' => $user->balloonContent,'clusterCaption' => $user->clusterCaption,'hintContent' => $user->hintContent,'iconCaption' => $user->iconCaption],
                'options' => ['preset' => $user->preset],
            ];
            $array[] = $arr1;

        }
        $arr['features'] = $array;
        $json = json_encode($arr, JSON_UNESCAPED_UNICODE);

        $file = fopen('data.json', 'w');
        // и записываем туда данные
        $write = fwrite($file,$json);
        //закрываем файл
        fclose($file);
        // проверяем успешность выполнения операции
        if($write) return "Данные успешно записаны!<br>";
        else return "Не удалось записать данные!<br>";
    }
}





