<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImagesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        return [
//            //
//        ];
        foreach($this->request->file('images') as $key => $val){
            $rules['image.'.$key] = 'required|mimes:jpeg,png,jpg,gif|max:2048';
        }
        return $rules;
    }
}
